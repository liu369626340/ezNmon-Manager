package tdd.xuchun.test.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tdd.xuchun.test.dao.HostMapper;
import tdd.xuchun.test.model.Host;
import tdd.xuchun.test.model.Page;
import tdd.xuchun.test.service.IHostService;

@Service("hostService")
public class HostServiceImpl implements IHostService{
	
   

	@Autowired
	private HostMapper hostMapper;
	
	@Override
	public Host getHost(Host host){
		return hostMapper.getHost(host);
	}
	@Override
	public Host getHostByIP(Host host){
		return hostMapper.getHostByIP(host);
	}

	@Override
	public void addHost(Host host) {
		hostMapper.addHost(host);
	}
	
	@Override
	public Page queryLikeHosts(Map<String, Object> cond) {
		Page page=new Page();
		//根据条件查询符合的主机列表记录总数，赋值给page的totalNum变量
		page.setTotalNum(hostMapper.getLikeHostsCount(cond));
		//从请求参数中获取每页大小
		int pageSize = Integer.parseInt(String.valueOf(cond.get("pageSize")));
		page.setPageSize(pageSize);
		//从请求参数中获取当前页码
		int curPageNum = Integer.parseInt(String.valueOf(cond.get("pageNum")));
		page.setCurPageNum(curPageNum);
		//动态计算总页数(总记录数 除以 每页大小，加上  总页数 求余 每页大小，如果余数不为0，则 加 1，否则 加 0 )
		page.setTotalPage(page.getTotalNum()/pageSize+(page.getTotalNum()%pageSize==0?0:1));
		//根据条件查询符合的主机列表记录，赋值给page的result变量
		page.setResult(hostMapper.getLikeHosts(cond));
		
		return page;
	}

	@Override
	public void delHost(Host host) {
		hostMapper.delHost(host);
	}

	@Override
	public void editHost(Host host) {
		hostMapper.editHost(host);		
	}
	
}
