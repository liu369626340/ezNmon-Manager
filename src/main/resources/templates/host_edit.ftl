﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>编辑用户</title>
	<link rel="stylesheet" href="${request.contextPath}/css/bootstrap.min.css">
	<script src="${request.contextPath}/js/jquery/jquery.min.js"></script>
	<script src="${request.contextPath}/js/jquery.validation/1.14.0/jquery.validate.min.js"></script>
	<script src="${request.contextPath}/js/jquery.validation/1.14.0/messages_zh.min.js"></script>
	<style>
		input.error{
			border: 1px solid #E6594E;
		}
	</style>
</head>
<body>
    <div class="container">
    <form id="editHostForm">
		  <br/>
		  <div class="form-group">
			<label for="hostname">主机名称：</label>
			<input type="text" class="form-control" id="hostname" name="hostname"  value="${data.name}" placeholder="输入主机名称">
		  </div>
		  <div class="form-group">
			<label for="hostaddr">主机IP：</label>
			${data.addr}<input type="hidden" id="hostaddr" name="hostaddr"  value="${data.addr}">
		  </div>
		   <div class="form-group">
			<label for="hostaddr">SSH端口号：</label>
			<input type="text" class="form-control" id="hostport" name="hostport" value="${data.port}" placeholder="输入SSH端口号">
		  </div>
		  <div class="form-group">
			<label for="hostusername">主机用户名：</label>
			<input type="text" class="form-control" id="hostusername" name="hostusername"  value="${data.username}" placeholder="主机用户名">
		  </div>
		<div class="form-group">
			<label for="hostpassword">密钥文件（填写此项则用户密码不生效）：</label>
			<input type="file" class="form-control" id="hostkeyfile" name="hostpkeyfile" placeholder="密钥文件" value="${data.keyfilepath}"  ><input type="button" value="上传" onclick="submit2();" />
			<input type="input" class="form-control" id="hostkeyfilepath" name="hostpkeyfilepath"  value="${data.keyfilepath}"  readonly="readonly" >
		</div>
		<div class="form-group">
			<label for="hostpassword">用户密码（若密钥有密码则填写在此处）：</label>
			<input type="password" class="form-control" id="hostpassword" name="hostpassword"  value="${data.password}" placeholder="主机密码">
		</div>
		   <div class="form-group">
				<label for="eznmonport">监控端口号：</label> <input type="text"
					class="form-control" id="eznmonport" name="eznmonport" value="${data.eznmonport}"
					placeholder="输入EasyNmon端口号">
			</div>
		  <div class="form-group">
				<label>系统类型：（默认nmon支持CentOS6~7等，Ubuntu和SUSE等版本需要选择）</label> <select id="ostype" name="ostype"  class="form-control">
					<option value="">--请选择--</option>
				</select>
				<input type="hidden" id="hostype" name="hostype"  value="${data.ostype}" >
			</div>
		   <div class="form-group">
			<label for="hosteremark">主机说明：</label>
			<input type="text" class="form-control" id="hostremark" name="hostremark"  value="${data.remark}" placeholder="主机说明">
		  </div>
		  <div class="form-group">
		  <button type="button" id="saveBtn" class="btn btn-success">提交</button>
		  <button type="button" id="cancelBtn" class="btn btn-default">取消</button>
		  </div>
		</form>
    </div>

		<script>
			function submit2(){
				var type = "file";          //后台接收时需要的参数名称，自定义即可
				var id = "hostkeyfile";            //即input的id，用来寻找值
				var formData = new FormData();
				formData.append(type, $("#"+id)[0].files[0]);    //生成一对表单属性
				$.ajax({
					type: "POST",           //因为是传输文件，所以必须是post
					url: '/upload',         //对应的后台处理类的地址
					data: formData,
					processData: false,
					contentType: false,
					success: function (data) {
						var msg=data.split("|")
						//alert(msg);
						alert(msg[0]);
						if(msg.length>1){
							$("#hostkeyfilepath").val(msg[1]);
						}
					}
				});
			}
		function getContextPath(){
		    return "${request.contextPath}";
		}
		var contextPath = getContextPath();
			var editHost = function(){
				if(!check().form()){ 
					return;  
				}
				$.ajax({
					   type: "GET",
					   dataType: "json",
					   url: contextPath+"/host/edit",
					   data: {
					        "id": ${data.id},
							"name": $("#hostname").val(),
							"addr":$("#hostaddr").val(),
							"port":$("#hostport").val(),
							"username":$("#hostusername").val(),
							"password":$("#hostpassword").val(),
							"remark":$("#hostremark").val(),
							"eznmonport":$("#eznmonport").val(),
							"ostype":$("#ostype").val(),
						    "keyfilepath" : $("#hostkeyfilepath").val()

					   },
					   success: function(msg){
							$('#cancelBtn').click();			
					   }
				});
			}
			
			$('#saveBtn').on('click', function(){
				editHost();
			});
			
			$('#cancelBtn').on('click', function(){
				var index = parent.layer.getFrameIndex(window.name);
				parent.getHostPageList();
				parent.layer.close(index);
			});

			
			//校验字段是否正确 
           function check(){ 
                /*返回一个validate对象，这个对象有一个form方法，返回的是是否通过验证*/ 
                return $("#editHostForm").validate({ 
                            rules:{ 
                                hostname:{ 
                                    required:true
                                },
                                hostaddr:{ 
                                    required:true                                   
                                },
                                hostport:{ 
                                    required:true                                   
                                },
								hostusername:{
									required:true
								}
                            }, 
                            messages:{ 
                                hostname:{ 
                                    required:""
                                },
                                hostaddr:{ 
                                    required:""                                
                                },
                                hostport:{ 
                                    required:""                                
                                },
								hostusername:{
									required:""
								}

                            }     
                        }); 
            }
            
            function getOSList() {
			$.ajax({
				type : "GET",
				dataType : "json",
				url : contextPath + "/easynmon/getostype",
				data : {},
				success : function(msg) {
					$('#ostype').html('');
					var options = '';
					options += '<option value="">--请选择--</option>';
					var pair = msg.message.split(",");
					for (var i = 0; i < pair.length; i++) {
						options += '<option value="' + pair[i] + '">' + pair[i]
								+ '</option>';
					}
					$('#ostype').append(options);
					//alert(document.getElementById('hostype').value);
					document.getElementById('ostype').value=document.getElementById('hostype').value;
				}
			});
		}
		 getOSList();
		
		</script> 	
</body>
</html>